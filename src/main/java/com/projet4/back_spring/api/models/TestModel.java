package com.projet4.back_spring.api.models;

public class TestModel {

	private int Id;
	
	private String mail;

	public TestModel(int id, String mail) {
		super();
		Id = id;
		this.mail = mail;
	}

	public TestModel() {}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
}
