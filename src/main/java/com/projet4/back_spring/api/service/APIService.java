package com.projet4.back_spring.api.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.projet4.back_spring.api.models.TestModel;

@Service
public class APIService {

	private List<TestModel> list;
	
	public APIService() {
		list = new ArrayList<>();
		
		TestModel model1 = new TestModel(1, "Hello from Spring back");
		TestModel model2 = new TestModel(2, "Woops 2 far");
		
		list.addAll(Arrays.asList(model1,model2));
	}
	
	
	public TestModel getModel(int id) {
		TestModel result = new TestModel();
		for (TestModel testModel : list) {
			if(id==testModel.getId()) {
				result = testModel;
			}
		}
		
		return result;
	}

}
