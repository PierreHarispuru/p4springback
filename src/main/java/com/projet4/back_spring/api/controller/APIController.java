package com.projet4.back_spring.api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.projet4.back_spring.api.models.TestModel;
import com.projet4.back_spring.api.service.APIService;

@RestController
@CrossOrigin(origins = "*")
public class APIController {
	
	private APIService service;
	
	public APIController(APIService service) {
		this.service = service;
	}
	
	@GetMapping("/spring")
	public TestModel getModel(@RequestParam int id) {
		return service.getModel(id);
	}

}
