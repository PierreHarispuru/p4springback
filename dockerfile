FROM maven:latest

WORKDIR /app

COPY /pom.xml .
COPY src/ ./src 

RUN mvn package -DskipTests

RUN cp target/*.jar app.jar

CMD ["java", "-jar", "app.jar"]
